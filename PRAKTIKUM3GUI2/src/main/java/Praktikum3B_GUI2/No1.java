/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Praktikum3B_GUI2;
import java.awt.Color;
import java.awt.Container;
import java.awt.Frame;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


public class No1 extends JFrame {
    private static final int FRAME_WIDTH =300;
    private static final int FRAME_HEIGHT =200;
    private static final int FRAME_X_ORIGIN =300;
    private static final int FRAME_Y_ORIGIN =100;
    private static final int BUTTON_WIDTH =80;
    private static final int BUTTON_HEIGHT =40;
    private JLabel label_nama;
    private JLabel label_jenisKelamin;
    private JLabel label_hobi;
    private JTextField text_nama;
    private JRadioButton chekLaki;
    private JRadioButton chekPerempuan;
    private JCheckBox chekOlahraga;
    private JCheckBox chekShopping;
    private JCheckBox chekComputerGames;
    private JCheckBox chekNontonBioskop;
    private JButton cancelButton;
    private JButton okButton;
    private JButton txtField;
    
    public static void main(String[] args) {
        No1 frame = new No1();
        frame.setVisible(true);
    }
    public No1(){
        Container contentPane= getContentPane();
        
        setSize(400, 400);
        setResizable(true);
        setTitle("Program Latihan 1");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        
        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);
        
        label_nama =new JLabel("Nama \t :");
        label_nama.setBounds(20, 14, 100, 50);
        this.add(label_nama);
        text_nama = new JTextField();
        text_nama.setBounds(120, 30, 180, 20);
        this.add(text_nama);
        
        label_jenisKelamin = new JLabel("Jenis Kelamin \t: ");
        label_jenisKelamin.setBounds(20, 40, 100, 50);
        this.add(label_jenisKelamin);
        
        chekLaki = new JRadioButton("Laki-laki");
        chekLaki.setBounds(120, 55, 80, 20);
        this.add(chekLaki);
        
        chekPerempuan = new JRadioButton("Perempuan");
        chekPerempuan.setBounds(210, 55, 90, 20);
        this.add(chekPerempuan);
        
        label_hobi = new JLabel("Hobi \t: ");
        label_hobi.setBounds(20, 65, 100, 50);
        this.add(label_hobi);
        
        chekOlahraga = new JCheckBox("Olahraga");
        chekOlahraga.setBounds(120, 80, 90, 20);
        this.add(chekOlahraga);
        
        chekShopping = new JCheckBox("Shopping");
        chekShopping.setBounds(120, 105, 90, 20);
        this.add(chekShopping);
        
        chekComputerGames = new JCheckBox("Computer Games");
        chekComputerGames.setBounds(120, 130, 130, 20);
        this.add(chekComputerGames);
        
        chekNontonBioskop = new JCheckBox("Nonton Bioskop");
        chekNontonBioskop.setBounds(120, 155, 130, 20);
        this.add(chekNontonBioskop);
        
        okButton = new JButton("OK");
        okButton.setBounds(100, 200, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        cancelButton=new JButton("Cancel");
        cancelButton.setBounds(200, 200, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}