package Praktikum3C_GUI2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class No1 extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;

    public static void main(String[] args) {
       new No1();
        
    }

    public No1() {
          JMenu File,Edit;  
         Container contentPane;
          JFrame frame= new JFrame("No1");  
          JMenuBar mb=new JMenuBar();  
          File=new JMenu("File");  
          Edit=new JMenu("Edit"); 
          
         contentPane = getContentPane();
            contentPane.setBackground(Color.pink);
            contentPane.setLayout(new FlowLayout());
          mb.add(File);  
          mb.add(Edit);  
          frame.setJMenuBar(mb);  
          frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);  
          frame.setContentPane(contentPane);
          frame.setVisible(true);  
          setDefaultCloseOperation(EXIT_ON_CLOSE);

}}
