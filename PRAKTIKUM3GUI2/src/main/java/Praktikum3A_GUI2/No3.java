/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Praktikum3A_GUI2;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class No3 extends JFrame {
    public No3(){
         this.setSize(300,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel=new JPanel();
        JButton tombol = new JButton();
        tombol.setText("ini Tombol");
        panel.add(tombol);
        this.add(panel);

        JLabel label=new JLabel("labelku");
        panel.add(label);
        this.add(panel);
        
        JTextField text=new JTextField("Text Field");
        panel.add(text);
        this.add(panel);
        
        JCheckBox box=new JCheckBox("Check box");
        panel.add(box);
        this.add(panel);
        
        JRadioButton radio=new JRadioButton("Radio Button");
        panel.add(radio);
        this.add(panel);
    }
    public static void main(String[] args) {
        new No3();
    }}